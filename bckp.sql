PGDMP     :                    z           test     14.5 (Ubuntu 14.5-1.pgdg22.04+1)     14.5 (Ubuntu 14.5-1.pgdg22.04+1)     &           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            '           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            (           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            )           1262    16386    test    DATABASE     Y   CREATE DATABASE test WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';
    DROP DATABASE test;
                postgres    false            *           0    0    DATABASE test    ACL     $   GRANT ALL ON DATABASE test TO omid;
                   postgres    false    3369            9           1247    24896    enum_doctorVisits_status    TYPE     j   CREATE TYPE public."enum_doctorVisits_status" AS ENUM (
    'pending',
    'confirmed',
    'canceled'
);
 -   DROP TYPE public."enum_doctorVisits_status";
       public          omid    false            �            1259    24913    admins    TABLE     �   CREATE TABLE public.admins (
    id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL
);
    DROP TABLE public.admins;
       public         heap    omid    false            �            1259    24912    admins_id_seq    SEQUENCE     �   CREATE SEQUENCE public.admins_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.admins_id_seq;
       public          omid    false    210            +           0    0    admins_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.admins_id_seq OWNED BY public.admins.id;
          public          omid    false    209            �            1259    24948    doctorVisits    TABLE     6  CREATE TABLE public."doctorVisits" (
    id integer NOT NULL,
    "fullName" character varying(255) NOT NULL,
    description text NOT NULL,
    "phoneNumber" character varying(255) NOT NULL,
    "visitTime" date NOT NULL,
    status public."enum_doctorVisits_status" NOT NULL,
    created_at date NOT NULL
);
 "   DROP TABLE public."doctorVisits";
       public         heap    omid    false    825            �            1259    24947    doctorVisits_id_seq    SEQUENCE     �   CREATE SEQUENCE public."doctorVisits_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."doctorVisits_id_seq";
       public          omid    false    212            ,           0    0    doctorVisits_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public."doctorVisits_id_seq" OWNED BY public."doctorVisits".id;
          public          omid    false    211            �           2604    24916 	   admins id    DEFAULT     f   ALTER TABLE ONLY public.admins ALTER COLUMN id SET DEFAULT nextval('public.admins_id_seq'::regclass);
 8   ALTER TABLE public.admins ALTER COLUMN id DROP DEFAULT;
       public          omid    false    209    210    210            �           2604    24951    doctorVisits id    DEFAULT     v   ALTER TABLE ONLY public."doctorVisits" ALTER COLUMN id SET DEFAULT nextval('public."doctorVisits_id_seq"'::regclass);
 @   ALTER TABLE public."doctorVisits" ALTER COLUMN id DROP DEFAULT;
       public          omid    false    212    211    212            !          0    24913    admins 
   TABLE DATA           8   COPY public.admins (id, username, password) FROM stdin;
    public          omid    false    210   �       #          0    24948    doctorVisits 
   TABLE DATA           u   COPY public."doctorVisits" (id, "fullName", description, "phoneNumber", "visitTime", status, created_at) FROM stdin;
    public          omid    false    212   �       -           0    0    admins_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.admins_id_seq', 1, true);
          public          omid    false    209            .           0    0    doctorVisits_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public."doctorVisits_id_seq"', 3, true);
          public          omid    false    211            �           2606    24920    admins admins_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.admins
    ADD CONSTRAINT admins_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.admins DROP CONSTRAINT admins_pkey;
       public            omid    false    210            �           2606    24955    doctorVisits doctorVisits_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public."doctorVisits"
    ADD CONSTRAINT "doctorVisits_pkey" PRIMARY KEY (id);
 L   ALTER TABLE ONLY public."doctorVisits" DROP CONSTRAINT "doctorVisits_pkey";
       public            omid    false    212            !      x�3�LL����442"�=... 3�+      #   e   x���1
�0D�z�.��]�x�`"�@
OoJA��<�rl��r�����( �!�3��׭)�B��;˟�ogT�Z�%� ��f/����;f���)�     