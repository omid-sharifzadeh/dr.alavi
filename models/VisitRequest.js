const Sequelize = require("sequelize");
const sequelize = require("../connection");

const DoctorVisit = sequelize.define(
  "doctorVisits",
  {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      allowNull: false,
      primaryKey: true,
    },
    fullName: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false,
    },
    phoneNumber: {
      type: Sequelize.STRING,
      allowNull: false,
    },
    visitTime: {
      type: Sequelize.DATEONLY,
      allowNull: false,
    },
    status: {
      type: Sequelize.ENUM(["pending", "confirmed", "canceled"]),
      allowNull: false,
    },
    created_at: {
      type: Sequelize.DATEONLY,
      defaultValue: Sequelize.NOW,
      allowNull: false,
    },
  },
  {
    timestamps: false,
  }
);

async () => {
  await DoctorVisit.sync({ alter: true });
};

module.exports = DoctorVisit;
