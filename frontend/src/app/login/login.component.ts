import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { LoginService } from '../services/login.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  showAlert: boolean = false;

  constructor(
    private service: LoginService,
    private cookie: CookieService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  sendForm(form : NgForm) {
    if(form.invalid){
      this.showAlert = true
      return;
    }
    this.service
      .sendLoginForm({
        username: form.value.username,
        password: form.value.password
      })
      .subscribe(
        (output: any) => {
          if (output.token) {
            this.cookie.set('Authorization', output.token, 1);
            this.router.navigate(['managment']);
          } else {
            this.showAlert = true;
          }
        },
        (error) => {
          console.log('failed !', error);
          this.showAlert = true;
        }
      );
  }
}
