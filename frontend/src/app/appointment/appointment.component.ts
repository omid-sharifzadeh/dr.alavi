import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { VisitRequestsService } from '../services/visit-requests.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css'],
})
export class AppointmentComponent implements OnInit {
  dateValue = new FormControl();

  token: string = this.cookie.get('Authorization');

  showSuccessAlert: boolean = false;
  showErrorAlert: boolean = false;

  constructor(
    private service: VisitRequestsService,
    private cookie: CookieService
  ) {}

  ngOnInit(): void {}

  sendForm(form: NgForm) {
    if (form.invalid) {
      this.showErrorAlert = true;
      return;
    }
    this.service
      .newVisitRequest({
        name: form.value.name,
        description: form.value.description,
        date: this.dateValue.value,
        phoneNumber: form.value.number,
        token: this.token,
      })
      .subscribe(
        (output) => {
          this.showSuccessAlert = true;
          this.showErrorAlert = false;
        },
        (error) => {
          console.log(error);
          alert('مشکلی پیش امده ! درخواست ثبت نشد.');
        }
      );
  }
}
