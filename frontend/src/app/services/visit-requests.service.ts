import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class VisitRequestsService {
  constructor(private http: HttpClient) {}

  // connecting to api end points
  newVisitRequest(data: any) {
    return this.http.post('http://127.0.0.1:8000/api/newVisitRequest', data, {
      headers: new HttpHeaders({ Authorization: data.token }),
    });
  }
  getAllVisitRequests(data: any) {
    return this.http.get('http://127.0.0.1:8000/api/getVisitRequests', {
      headers: new HttpHeaders({ Authorization: data }),
    });
  }
  updateVisitRequest(data: any) {
    return this.http.put(
      'http://127.0.0.1:8000/api/updateVisitStatus/' + data.id,
      { status: data.status },
      { headers: new HttpHeaders({ Authorization: data.token }) }
    );
  }
}
