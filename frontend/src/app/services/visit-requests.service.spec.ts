import { TestBed } from '@angular/core/testing';

import { VisitRequestsService } from './visit-requests.service';

describe('VisitRequestsService', () => {
  let service: VisitRequestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisitRequestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
