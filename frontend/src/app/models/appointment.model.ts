export interface AppointmentInterface {
  name: string;
  description: string;
  date: string;
  phoneNumber: string;
}
export interface AppointmentResponseInterface {
  message: string;
}
