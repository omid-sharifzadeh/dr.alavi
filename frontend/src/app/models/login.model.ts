export interface LoginInterface {
  username: string;
  password: string;
}
export interface LoginResponseInterface {
  status: string
}
