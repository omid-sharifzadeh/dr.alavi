import { Component, OnInit } from '@angular/core';
import { VisitRequestsService } from '../services/visit-requests.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css'],
})
export class ManagementComponent implements OnInit {
  visitRequests: any = false;
  token: string = this.cookie.get('Authorization');

  constructor(
    private service: VisitRequestsService,
    private cookie: CookieService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {
    // get visit requests from api
    this.service.getAllVisitRequests(this.token).subscribe(
      (data) => {
        this.visitRequests = data;
      },
      (error) => {
        this.router.navigate(['login']);
      }
    );
  }

  ngOnInit(): void {}
  // snack bar animation
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, { duration: 5000, direction: 'rtl' });
  }

  // change visit request status to confirmed
  confirmRequest(id: Number) {
    this.service
      .updateVisitRequest({ id: id, status: 'confirmed', token: this.token })
      .subscribe(
        (data) => {
          this.service.getAllVisitRequests(this.token).subscribe((data) => {
            this.visitRequests = data;
          });
          this.openSnackBar(
            'عملیات با موفقیت انجام شد',
            'بستن'
          );
        },
        (error) => {
          this.openSnackBar('درخواست شما با خطا مواجه شد!', 'بستن');
        }
      );
  }

  // change visit request status to canceled
  cancelRequest(id: number) {
    this.service
      .updateVisitRequest({ id: id, status: 'canceled', token: this.token })
      .subscribe(
        (data) => {
          this.service.getAllVisitRequests(this.token).subscribe((data) => {
            this.visitRequests = data;
          });
          this.openSnackBar(
            'عملیات با موفقیت انجام شد',
            'بستن'
          );
        },
        (error) => {
          this.openSnackBar('درخواست شما با خطا مواجه شد!', 'بستن');
        }
      );
  }
}
