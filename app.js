require("dotenv").config();

const express = require("express");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const db = require("./connection");
const app = express();
const PORT = process.env.PORT || 3000;

const visitRoutes = require("./routes/visitRequests");
const loginRoutes = require("./routes/login");

db.sync()
  .then((result) => {
    console.log("Database Connected !");
  })
  .catch((err) => {
    console.log("Database Error !",err);
  });

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PUT, DELETE, OPTIONS"
  );
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.json());
app.use("/api", visitRoutes);
app.use(loginRoutes);

app.listen(PORT, () => {
  console.log(`server is listening at ${PORT}`);
});
