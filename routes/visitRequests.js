const express = require("express");
const router = express.Router();

const visitRequestsController = require("../controllers/visitRequests");

const checkAuth = require("../middlewere/auth");

router.get(
  "/getVisitRequests",
  checkAuth,
  visitRequestsController.all_visit_requests
);

router.post(
    "/newVisitRequest", 
    visitRequestsController.new_visit_request
);

router.put(
  "/updateVisitStatus/:id",
  checkAuth,
  visitRequestsController.update_visit_request
);

module.exports = router;
