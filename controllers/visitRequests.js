const DoctorVisit = require("../models/VisitRequest");

exports.all_visit_requests = (req, res) => {
  DoctorVisit.findAll({order: [['id', 'DESC']]})
    .then((doctorVisits) => {
      res.json(doctorVisits);
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};

exports.new_visit_request = (req, res) => {
  DoctorVisit.create({
    fullName: req.body.name,
    description: req.body.description,
    phoneNumber: req.body.phoneNumber,
    visitTime: req.body.date,
    status: "pending",
  })
    .then(() => {
      res.status(201).json({ message: "success" });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};

exports.update_visit_request = (req, res) => {
  DoctorVisit.update(
    {
      status: req.body.status,
    },
    {
      where: {
        id: req.params.id,
      },
    }
  )
    .then(() => {
      res.json({ message: 'success' });
    })
    .catch((err) => {
      res.status(400).json({ message: err.message });
    });
};
