require("dotenv").config();

const jwt = require("jsonwebtoken");
const Admin = require("../models/admin");

// user login opration
exports.login = (req, res) => {
  Admin.findOne({
    where: { username: req.body.username, password: req.body.password },
  })
    .then((admin) => {
      // check for user exist
      if (admin != null) {
        // create token for user authentication
        const token = jwt.sign(
          {
            username: admin.username,
            password: admin.password,
          },
          process.env.JWT_SECRET_KEY,
          {
            expiresIn: "24h",
          }
        );
        res.json({ message: "success" , token : token});
      } else {
        res.status(401).json({ message: "auth failed!" });
      }
    })
    .catch((err) => {
      res.status(401).json({ message: "auth failed!" });
    });
};
