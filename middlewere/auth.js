require("dotenv").config();

const jwt = require("jsonwebtoken");
const Admin = require("../models/admin");

module.exports = (req, res, next) => {
  try {
    // get user data from cookie
    const token = req.headers.authorization;
    const userData = jwt.verify(token, process.env.JWT_SECRET_KEY);
    // check for user authorization
    Admin.findOne({
      where: { username: userData.username, password: userData.password },
    })
      .then((admin) => {
        if (admin != null) {
          next();
        } else {
          res.status(401).json({ message: "auth failed !" });
        }
      })
      .catch((err) => {
        res.status(401).json({ message: "auth failed !" });
      });
  } catch (error) {
    return res.status(401).json({
      message: 'auth failed !',
    });
  }
};
